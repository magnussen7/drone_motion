#!/usr/bin/python3
# coding: utf-8
import re
import matplotlib.pyplot as plt

regex_acceleration = '\[drone\]\(DEBUG\)> accel: (?P<acceleration>[-+]?\d)'
regex_coord = '\[drone\]\(DEBUG\)> dir: \(x=(?P<x>[-+]?\d*\.\d+|\d+),y=(?P<y>[-+]?\d*\.\d+|\d+),z=(?P<z>[-+]?\d*\.\d+|\d+)\)'

x_coord = []
y_coord = []
z_coord = []
x = 0
y = 0
z = 0

with open('sensors.log', 'r') as source:
    for line in source.readlines():
        match_coord = re.match(regex_coord, line)

        if match_coord != None:
            x+= float(match_coord.group("x"))
            y+= float(match_coord.group("y"))
            z+= float(match_coord.group("z"))
            x_coord.append(x)
            y_coord.append(y)
            z_coord.append(z)

    ax = plt.plot(x_coord, y_coord, '-')

    plt.axis('equal')
    plt.savefig('ouput.png', dpi=1000)
